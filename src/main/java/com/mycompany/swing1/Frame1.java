/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swing1;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author BOAT
 */
public class Frame1 {
    public static void main(String[] args) {
         JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300); // Overloading method
        
        JLabel lblHelloWorld = new JLabel("Hello World!", JLabel.CENTER);
        lblHelloWorld.setBackground(Color.pink);
        lblHelloWorld.setOpaque(true);
        lblHelloWorld.setFont(new Font("SansSerif", Font.PLAIN, 25));
        frame.add(lblHelloWorld);
        
        frame.setVisible(true); 
        
    }
    }

